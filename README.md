INTRODUCTION
------------
Video Embed Screencast is a submodule of Video Embed Field module. 
It creates a simple field type that enables you to embed videos 
from Screencast.com simply by entering Screencast.com video URL.

 - [Screencast](http://www.screencast.com/)

REQUIREMENTS
------------

This module requires the following modules:

 * Drupal 8.x
 * [Video Embed Field](https://www.drupal.org/project/video_embed_field)

INSTALLATION
------------

##### Drupal 8.x


```sh
 composer require drupal/video_embed_screencast
```

Content-types -> Manage Fields -> Add field -> FIELD TYPE: Video Embed

> Module is not compatible with older Drupal versions
